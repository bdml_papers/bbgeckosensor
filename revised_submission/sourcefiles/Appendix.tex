\appendix
\section*{Appendix: Tactile sensor materials and methods}
\setcounter{section}{1}

\subsection{Sensor design}

	The optical tactile sensor described in section~\ref{sec:TactileSensor} uses a rectangular array of microscale pyramidal taxels with an included angle of 70.5~degrees, as shown in figure~\ref{fig:taxelgeometry}. An included angle of about 70~degrees has been found to produce a nearly linear response of light intensity to stress for conical taxels \cite{Ohka2012}, but nonlinear taxel response curves are used here for greater accuracy (see \ref{sec:calibration}). The taxel array was manufactured by casting PDMS into an Si wafer etched by potassium hydroxide \cite{Mannsfeld2010}.

\begin{figure}
	\centering
	\includegraphics[]{FigureA1.pdf}
	\caption{Macro photograph of the taxel array on the PDMS sensing membrane. Inset: drawing showing the measured dimensions of the taxels.}
	\label{fig:taxelgeometry}
\end{figure}

	The PDMS membrane is about 80~$\mu$m thick, a compromise chosen to minimize coupling between taxels while still thick enough to demold from the Si wafer without tearing. The PDMS used (Dow Corning Sylgard 170) is normally opaque, but the membrane is slightly translucent due to its thinness. This can cause spurious signals when light passes through the sensor. This source of error was minimized by conducting experiments in a darkened room, although a small amount of stray light was produced by the sensor itself.

	The light from the taxels is collected by a camera with a macro lens (Panasonic Lumix DMC-GH3, Olympus M.Zuiko 60~mm f/2.8), which records video at 1920$\times$1080 pixel resolution at 60~Hz. The field of view of the camera covers an area of about 46$\times$26~mm of the sensor. Roughly 118\,800 taxels are visible in the field of view, with each taxel taking up about 4$\times$4 camera pixels. This allows the signals from each taxel to be easily distinguished.

\subsection{Sensor calibration}
\label{sec:calibration} 

	The sensor was calibrated by acquiring images at varying vacuum pressures with no applied forces. The vacuum pressure was independently measured by a digital vacuum gauge. Independent brightness values for each taxel were extracted from the camera images (see \ref{sec:dataanalysis}) and these data were fitted to least squares cubic polynomial calibration curves to convert between brightness and normal stress. The calibration curves for each taxel were computed independently to account for taxel imperfections. As shown in figure~\ref{fig:calibrationcurves}, the cubic polynomials fit the calibration data well for most taxels, even when the taxel response falls outside the typical range.

\begin{figure}
	\centering
	\includegraphics[]{FigureA2.pdf}
	\caption{Calibration data and cubic fits for a typical taxel (green triangles), a taxel appearing brighter due to a trapped dust particle (red crosses), and taxels appearing brighter (blue circles) or darker (magenta squares) due to shape imperfections. The black dotted line and gray region show the median and 10th-90th percentile range of taxel response curves.}
	\label{fig:calibrationcurves}
\end{figure}

	The vacuum pressure during calibration was limited to pressures between 0 and 92~kPa due to the use of a vacuum pump. Therefore, the sensor is expected to be accurate only if the total normal stress is in this range: for example, if the differential vacuum pressure (atmospheric pressure minus absolute vacuum pressure) is set to 80~kPa, then the calibrated sensing range is $-80$ to +12~kPa. The calibration range could be extended by enclosing the apparatus in a pressure chamber to apply higher pressures during calibration, but this was not done in the present work.

\subsection{Image processing}   
\label{sec:dataanalysis}

	The raw data from the sensor is in video form, so image processing was used to obtain stress distribution measurements for each point in time. The process is illustrated in figure~\ref{fig:taxelcloseups}.

\begin{figure}
	\centering
	\includegraphics[]{FigureA3.pdf}
	\caption{Tactile sensor image processing. (\textit{a}) Raw image from the camera, with taxels visible as gray squares separated by black regions (non-contacting areas of the sensing membrane). The undesired moire pattern artifact can be seen as a periodic change in brightness between adjacent rows of taxels. (\textit{b}) Filtered image with moire pattern removed. (\textit{c}) Cyan squares denote the boundaries of the 4$\times$4 pixel neighborhoods centered on each taxel. (\textit{d}) Individual taxel signals, represented by squares, are obtained by calculating the average luminance over each 4$\times$4 pixel neighborhood.}
	\label{fig:taxelcloseups}
\end{figure}

	First, the frames of video were filtered. The camera introduced an undesired aliasing artifact by downsampling from the native resolution (4608$\times$3456) to the video resolution (1920$\times$1080). This caused cyclical variations in brightness across the images (moire patterns), as can be seen in figure~\ref{fig:taxelcloseups}(\textit{a}). These patterns slowly shift over time due to small changes in the distance from the camera to the sensor. The main component of this pattern has a wavelength of about 200~$\mu$m and was removed by applying a spatial filter to the camera images with a stopband from 1/240~$\mu$m$^{-1}$ to 1/170~$\mu$m$^{-1}$ (figure~\ref{fig:taxelcloseups}(\textit{b})).

	Next, independent brightness signals were extracted for each taxel in the field of view of the camera to achieve the maximum possible spatial resolution. Grayscale luminance values were computed from the gamma-encoded RGB color video components using the ITU-R BT.709 gamma transfer function \cite{BT.709-5}. Each taxel signal was then extracted by computing the average luminance over a square 4$\times$4~pixel neighborhood centered on the taxel's position (figures~\ref{fig:taxelcloseups}(\textit{c}) and~(\textit{d})). Since the taxel positions were not exactly aligned with the pixels, the luminance values in the taxel neighborhoods were calculated using bilinear interpolation. Stress values were then calculated for each taxel using the independent calibration curves.

	The taxel positions can move in-camera by small distances due to deflection of the entire sensor; they can also slide on the waveguide if they are subjected to high shear forces. For these reasons it is necessary to accurately track the coordinates of each taxel in the camera image at all points in time. This was done using digital image correlation \cite{Sutton2000} for each taxel independently. If tracking failed for any taxel due to detachment, that taxel's signal was discarded after that point in time.

	The moire pattern artifact introduced by the camera also had a secondary component with a wavelength that was comparable to the size scale of gecko lamellae. This could not be removed with spatial filtering without an adverse effect, so a temporal highpass filter with a time constant of 1.7~s was used to reduce the slowly-varying pattern while leaving faster signals unchanged. The effect of this filter was clamped so that the correction is limited to $\pm$2~kPa, limiting the systematic error introduced by the filtering.