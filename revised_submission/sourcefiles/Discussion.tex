\section{Discussion}
\label{sec:Discussion}

\subsection{Stress distribution and contact area on the gecko's toe}

	The maximum force produced by an adhesive is generally proportional to the area of contact or the number of adhesive bonds, so it would be reasonable to assume that the gecko maximizes the number of setae in contact to maximize its adhesive capability. However, the contact area measurements in section~\ref{sec:contactarea} show that a significant amount of the setal area does not make contact. In figure~\ref{fig:contactarea}, the most proximal lamellae are totally off the surface, and the more distal lamellae do not make contact over their entire width. \hl{Small dark patches within lamellae (figures~}\ref{fig:contactarea}\hl{(\textit{c}) and~}\ref{fig:contactfingerpress}\hl{) indicate that small sections of the lamellae do not make contact. These non-contacting regions appear to have a wrinkled or folded appearance. Severe lamellar wrinkling is seen on the 4th toe in figure~}\ref{fig:contactarea}\hl{(\textit{f})--(\textit{h}), where the sides of the toe appear to fold inwards as the toe slides on the surface. This severe folding may be associated with toe damage (the 4th toe is missing parts of the lamellae under the claw), but the smaller wrinkles seen in figures~}\ref{fig:contactarea}\hl{(\textit{c}) and~}\ref{fig:contactfingerpress}\hl{ were observed on all toes under load.}

	Lamellar wrinkling may be caused by the intrinsic curvature of the free toe surface. \hl{If the surface of the toe follows a surface with nonzero Gaussian curvature (i.e. a surface which cannot be wrapped by a flat sheet without stretching the sheet, such as a sphere), then flattening the toe pad against a flat surface could lead to wrinkles or folds.} The non-contacting regions of the lamellae detach from the surface even if they are pressed into contact, as shown in figure~\ref{fig:contactfingerpress}, due to the action of some restoring force. This elastic force associated with flattening the curved toe surface may be due to the bending stiffness of the skin or \hl{may be due to the structure of the tendons applying loads to those regions of the lamellae.}

	In the regions of lamellae that do make contact with the surface, the normal stress is not uniform. Figure~\ref{fig:lamellalineprofile} shows a stress distribution from a typical trial. As shown in the line profile in figure~\ref{fig:lamellalineprofile}(\textit{a}), a maximum tensile stress of $-60$~kPa is achieved on one lamella, while the others support smaller stresses. These typical values are not indicative of the maximum tensile stress of tokay gecko adhesive in general. Stronger tensile stresses were observed in some trials, but due to the delamination of the tactile membrane from the waveguide, stresses below about $-80$~kPa could not be recorded. 

\begin{figure}
	\centering
	\includegraphics[]{Figure16.pdf}
	\caption{Variation in stress between lamellae. (\textit{a}) Proximodistal line profile showing uneven normal stress distribution over 7 lamellae. Five independent profiles (gray) along parallel lines within the white rectangle in (\textit{b}) are averaged into a mean profile (black). (\textit{b}) Normal stress distribution from figure~\ref{fig:normalforcevsloadangle}(\textit{b}).}
	\label{fig:lamellalineprofile}
\end{figure}

	This evidence suggests that the system of setae, lamellae, and tendons in the gecko's toe does not achieve uniform distribution of loads (equal load-sharing). Instead, the highest tensile stresses are limited to a small fraction of the total area of the adhesive. The locations of these force concentrations can be anywhere on the toe and can vary between or within trials according to changes in the toe placement or loading angle.

	As a possible explanation for this behavior, we note that the tokay gecko is capable of supporting many times its own weight and therefore may not need the contact area and stress distribution to be perfect at all times. Additionally, the gecko's toe is not necessarily optimal for adhesion on flat surfaces. Gillies \emph{et al.} showed that tokay geckos adhere well to undulating rough surfaces \cite{Gillies2014}, which may be more common than perfectly flat surfaces in the geckos' natural habitat. We conjecture that the sub-optimal contact area and load distribution on flat surfaces may be the result of evolutionary adaptations to non-flat surfaces.

\subsection{Compressive stresses on lamellae}
\label{sec:compressive}

	The FTIR tactile sensor measurements suggest that some parts of lamellae can be under compressive stress (typically in the range 0--30~kPa) even though the overall normal force is tensile. This is not ideal for maximizing the adhesive force, because the compressive stress must be balanced by increased tensile stress in other areas. The distribution depends on the loading angle, as shown in figure~\ref{fig:normalforcevsloadangle}: significant compressive and tensile stresses are present for loading angles from 0 to 15~degrees, while the distribution becomes more tensile above 15~degrees. It is noted that some compressive stress values recorded by the sensor are outside the calibrated range and are therefore inaccurate, as discussed in section~\ref{sec:accuracy}, but some nonzero compressive stress must still have existed to produce these readings.

	The cause of this phenomenon is unknown, but we hypothesize that it is caused by interactions between neighboring lamellae. The lamellae are flexible and are assumed to have negligible bending stiffness, so they naturally tend to align with the external load, as depicted in figure~\ref{fig:lamellaecompression}(\textit{a}) for a loading angle above 15~degrees. However, for small loading angles, the lamellae cannot align with the load without crushing neighboring lamellae (figure~\ref{fig:lamellaecompression}(\textit{b})), which may explain the compressive stresses we observed in our experiments.

\begin{figure}
	\centering
	\includegraphics[]{Figure17.pdf}
	\caption{Hypothetical interaction between lamellae leading to compressive stresses at low loading angles (see figure~\ref{fig:geckocrosssection} for labels).}
	\label{fig:lamellaecompression}
\end{figure}

\subsection{Sliding}

	We observed continuous sliding without detachment in many trials when external loads were applied to the gecko. The steady-state sliding velocity typically ranged from 0.2 to 0.7~mm~s$^{-1}$ (larger velocities were also observed immediately before detachment). This occurred on both the PVC sensor cover membrane and the acrylic bare waveguide. Gecko setae are known to produce larger forces during sliding than during stable adhesion \cite{GravishWilkinsonAutumn2008,Gravish2010,Zhao2008,Puthoff2013}, but continuous sliding is believed to be rare under natural conditions \cite{Zhao2008, Gravish2010}. The slow steady-state sliding observed here appears to have a deleterious effect on the contact area as the lamellae become more wrinkled (figure~\ref{fig:contactarea}(\textit{d})--(\textit{h})) (see supplementary movie 2). This is probably caused by the branched lateral digital tendon system which tends to pull the sides of the toes inward (figure~\ref{fig:geckotendons}). \hl{It is unknown if the low-velocity sliding observed here was a result of the testing procedure or the individual gecko used as a test subject.}

\subsection{Issues with the measurements}

	The tactile sensor was designed for maximum spatial resolution, with the resulting loss of some degree of absolute accuracy in stress measurement. Foremost, the sensor tends to overestimate compressive stresses outside its calibration range (section~\ref{sec:accuracy}). Additional effects contribute to errors in stress readings, including the signals from moire patterns in the camera images (\ref{sec:dataanalysis}) and the small effects of taxel hysteresis and light leaking through the sensor membrane. These combined effects are estimated to cause an uncertainty of 5--10~kPa throughout the sensing range, while the calibration error was directly measured and is plotted in figure~\ref{fig:accuracyhysteresis}.

	Modifications to the sensor could ameliorate some of these issues. The hysteresis problem may be improved by using a different shape or material of taxel, and the light leakage problem may be solved by using a thicker sensor membrane (likely decreasing the spatial resolution). The moire patterns could be eliminated by using a scientific grade camera that does not downsample when recording video.

	Despite its inaccuracy, however, the sensor's relative sensitivity is good, showing clear distinctions between different levels of tensile and compressive normal stresses at high spatial resolution. The compressive stress values described in section~\ref{sec:compressive} are likely inaccurate, but are significant with respect to the sensor's uncertainty.

	Only one individual test subject was used, so the results are considered suggestive rather than conclusive. No attempt was made here to control for possible behavioral and physical changes associated with the gecko's molting cycle, nor anatomical variations between individual geckos.