\section{Sensor Testing}
\label{sec:SensorTesting}

	To characterize the FTIR tactile sensor, we performed a series of tests of accuracy and repeatability. Additionally, in order to increase confidence in the validity of the sensor readings, we used the sensor to reproduce some well-known results in the field of adhesion science: the stress distribution in a peeling strip of tape and the stress distribution for an elastic sphere in adhesive contact. Measurements of stress distributions on gecko toes are reported in section~\ref{sec:GeckoMeasurements}.

\subsection{Accuracy and hysteresis testing}
\label{sec:accuracy}

	The accuracy of the sensor was tested by placing test weights on it (to measure compressive stresses) and hanging test weights from it (to measure tensile stresses), using double-sided tape to attach the weights to the sensor. The measured stress was averaged over the contact area of the double-sided tape, and this was compared with the stress obtained by dividing the weight by the contact area. The results of this test are shown in figure~\ref{fig:accuracyhysteresis}(\textit{a}). \hl{The vacuum pressure was set to 80~kPa, the same vacuum pressure used in the gecko experiments, so the calibrated range extended from $-80$ to $+12$~kPa (see section~}\ref{sec:TactileSensor}\hl{).} Here negative values are tensile normal stresses and positive values are compressive normal stresses. The sensor was accurate within $\pm$1~kPa from $-54$~kPa to $-1$~kPa and within $\pm$3~kPa for the data points at $0$ and $7$~kPa, and the sensor became increasingly inaccurate for stresses of 14~kPa and above, which exceeded the calibrated range of the sensor. The accuracy was worse at the upper end of the calibrated range because the variation in stress over the contact area was large enough to extend into the uncalibrated range in some parts of the area.

\begin{figure}
	\centering
	\includegraphics[]{Figure05.pdf}
	\caption{Tactile sensor accuracy and hysteresis. (\textit{a}) Sensor readings vs.\ actual stress values for a vacuum differential pressure of 80~kPa (calibrated range $-80$ to +12~kPa). The dashed line of slope 1 represents perfect accuracy. Outside the calibrated range (shaded region), the sensor measurement becomes increasingly inaccurate. (\textit{b}) Normal stress error (sensor reading minus actual stress) vs.\ actual stress values for a tensile loading cycle (blue circles) and unloading cycle (red squares), showing hysteresis at zero applied stress. In the loading cycle, a compressive stress of +42~kPa was applied with a spring mechanism before a series of successively larger weights were hung from the sensor, reaching a maximum tensile stress of $-54$~kPa; in the subsequent unloading cycle, the tensile stress went from $-54$~kPa to 0 as the weight was sequentially reduced.}
	\label{fig:accuracyhysteresis}
\end{figure}

	The sensor also exhibits a small amount of hysteresis due to adhesion effects between the PDMS membrane and the acrylic waveguide. As a result, the sensor reading with zero applied stress can be different if it is measured following a large compressive or tensile stress. This hysteresis was quantified by comparing the tensile accuracy of the sensor during a loading cycle and an unloading cycle. The resulting data (figure~\ref{fig:accuracyhysteresis}(\textit{b})) show a maximum discrepancy of 1.3~kPa, indicating that this is a small effect.

\subsection{Testing of spatial resolution}

	As a qualitative demonstration of the spatial resolution of the sensor, the force distribution under a human finger is shown in figure~\ref{fig:fingerprints}. The friction ridges, skin creases, and small breaks in the ridges caused by pores are visible. The shape of the pressure distribution depends on the angle of inclination the finger pad. Holding the pad parallel to the sensor produces a flat distribution (figure~\ref{fig:fingerprints}(\textit{a})), while the distribution becomes peaked towards the distal end as the angle of the distal phalanx is increased (figures~\ref{fig:fingerprints}(\textit{b}) and~(\textit{c})), in agreement with previous measurements \cite{JohanssonFlanagan2009, Derler2013, PawlukHowe1999}. \hl{A vacuum pressure of 50~kPa was used in this test, with an associated calibrated range of $-50$ to $+42$~kPa, as explained in section~}\ref{sec:TactileSensor}\hl{. This vacuum pressure was chosen because large tensile stresses were not expected to occur in this test. In all other tests and experiments in this paper, the vacuum pressure was set to 80--81~kPa because adhesion was expected to play a larger role. A higher vacuum pressure causes the sensor to be more inaccurate for compressive stresses, but allows measurement of higher maximum tensile stresses (see section~}\ref{sec:TactileSensor}\hl{).} 

\begin{figure}
	\centering
	\includegraphics[]{Figure06.pdf}
	\caption{Normal stress distributions on a human fingertip measured using the FTIR tactile sensor. As the angle of the distal phalanx increases (\textit{a}--\textit{c}), the stress distribution becomes peaked toward the distal end. In this test the differential vacuum pressure was 50~kPa (calibrated range $-50$ to $+42$~kPa). ND = no data.}
	\label{fig:fingerprints}
\end{figure}

\subsection{Measurement of tape peeling forces}

	A strip of pressure-sensitive tape (3M Scotch 811 Removable Tape) was attached to the sensor and slowly peeled off at a 90~degree angle, so that detachment occurred in the interface between the tape adhesive and the sensor. This tape has a 50~$\mu$m thick cellulose acetate backing and a 10~$\mu$m thick acrylic-based adhesive layer with a slightly bumpy texture (equivalent to 3M Post-it Note adhesive) and was chosen to keep the tensile stresses below the sensor detachment limit. As shown in figure~\ref{fig:tapepeeling}(\textit{a}), the stress distribution shows a compressive zone ahead of the tensile peel zone. An average line profile was calculated by averaging the 2-dimensional stress measurement over its width, smoothing out the effect of the bumpy texture of the adhesive. This 1-dimensional stress distribution is shown in figure~\ref{fig:tapepeeling}(\textit{b}). Shown for comparison in figures~\ref{fig:tapepeeling}(\textit{c}) and (\textit{d}) are tape-peeling stress distributions measured by Niesiolowski and Aubrey using the curvature of the tape backing \cite{NiesiolowskiAubrey1981} and by Kaelble using a special transducer \cite{Kaelble1965}. These experiments used stronger tapes, but the results are qualitatively similar.

\begin{figure}
	\centering
	\includegraphics[]{Figure07.pdf}
	\caption{Stress distribution measurements on peeling strips of tape. (\textit{a}) 2-dimensional normal stress distribution measured using the FTIR tactile sensor for {3M 811} tape (peel rate 0.2~mm~s$^{-1}$, peel angle 90~degrees). Bright yellow regions indicate compressive stresses, while dark blue regions indicate tensile stresses. In this test the differential vacuum pressure was 81~kPa (calibrated range $-81$ to $+11$~kPa). (\textit{b}) 1-dimensional normal stress distribution calculated by sampling the 2-dimensional stress distribution along 50 equally spaced lines in a rectangular region centered on the peel zone (white rectangle in (\textit{a})). Black and gray lines indicate mean $\pm 1$~SD. (\textit{c}) Normal stress distribution (kPa) obtained by Niesiolowski and Aubrey for a 50~$\mu$m thick polyester tape with 50~$\mu$m thick polybutyl acrylate adhesive adhered to a glass surface (peel rate 0.33~mm~s$^{-1}$, peel angle 90~degrees) \cite{NiesiolowskiAubrey1981}. (\textit{d}) Normal stress distribution (kPa) obtained by Kaelble for a 43~$\mu$m thick cellophane tape with a 25~$\mu$m thick natural rubber-based adhesive adhered to a stainless steel surface (peel rate 0.083~mm~s$^{-1}$, peel angle 90~degrees) \cite{Kaelble1965}.}
	\label{fig:tapepeeling}
\end{figure}

\subsection{Measurement of adhesive contact of a rubber sphere}

	The adhesive contact between an elastic sphere and a rigid plane is an important problem in the study of adhesion and contact mechanics \cite{Israelachvili2011} and is also relevant to modeling of fibrillar adhesives such as gecko setae. In the theory developed by Johnson, Kendall, and Roberts \cite{JohnsonKendallRoberts1971}, the normal stress distribution as a function of radius $\sigma(r)$ is given by
\begin{equation}
\sigma(r) = \frac{3 E a}{2 \pi R}\left(1-\frac{r^2}{a^2}\right)^{1/2} - \left(\frac{3 E W}{2 \pi a}\right)^{1/2} \left(1-\frac{r^2}{a^2}\right)^{-1/2}, \label{eq:JKR}
\end{equation}
where $E$ is the Young's modulus of the sphere, $R$ is its radius, $a$ is the radius of the contact patch, and $W$ is the total effective interfacial energy per unit area. In steady state, $W$ is equivalent to the thermodynamic work of adhesion, which is roughly the sum of the surface energies of the elastic sphere and the rigid plane. However, for nonzero peel velocities, $W$ is expected to increase significantly due to velocity-dependent effects \cite{Lorenz2013}. \hl{This behavior can be described with a simple model:}
\begin{equation}
W = W_0 \left[1 + \left(\frac{v}{v_0}\right)^\alpha \right], \label{eq:velocitydependence}
\end{equation}
\hl{where $v = |\dot{a}|$ is the peel velocity, $W_0$ is the thermodynamic work of adhesion, and $v_0$ and $\alpha$ are power-law constants. The scaling exponent $\alpha$ is not a universal constant, but depends on the material properties and the velocity range }\cite{Lorenz2013}\hl{.}

	A smooth rubber sphere of radius $R = 24$~mm and modulus $E = 3$~MPa was pressed into the sensor and the normal stress distribution was measured over the contact patch as a function of time as the sphere was pulled off the sensor. The normal stresses were compressive over the entire contact patch as the sphere was pressed into the sensor, but tensile normal stresses were observed at the perimeter of the contact patch during pull-off (figure~\ref{fig:spherestress}). The distributions were nearly radially symmetric, so radial stress distributions were determined by taking a number of line profiles at different angles through the center of the patch and averaging them together ($n = 20$, \hl{angle between lines $= 9$~degrees}).

\begin{figure}
	\centering
	\includegraphics[]{Figure08.pdf}
	\caption{Normal stress distribution measurements for a rubber sphere in adhesive contact with the FTIR tactile sensor at four points in time during pull-off (calibrated range $-81$ to $+11$~kPa).}
	\label{fig:spherestress}
\end{figure}

	A series of successive radial distributions associated with decreasing patch radii $a$ are plotted in figure~\ref{fig:JKRmodel}, with the velocity-dependent JKR model (equations (\ref{eq:JKR}) and (\ref{eq:velocitydependence})) shown for comparison. The patch radii $a$ were assumed to be equal to the locations of the minima of the experimental stress distributions (since the theoretical distribution diverges to negative infinity at $r=a$). The thermodynamic work of adhesion between rubber and PVC was estimated to be roughly 70~mJ~m$^{-2}$.

	\hl{The peel velocity $v$ increased from 3.6 to 27~mm~s$^{-1}$ over the time 0--83~ms, causing the effective interfacial energy $W$ to be enhanced by a factor of 7--15. The curves in figure~}\ref{fig:JKRmodel}\hl{ are plotted using $W_0 = 70$~mJ~m$^{-2}$, $v_0 = 5\times10^{-5}$~m~s$^{-1}$, and $\alpha = 0.42$.} This is roughly consistent with previous results for the velocity-dependence of adhesion. Lorenz et al.\ found experimentally that the effective interfacial energy was enhanced by about a factor of 16 for a PDMS ball pulled off a flat substrate with crack propagation velocity 10~mm~s$^{-1}$ \cite{Lorenz2013}; this was attributed to viscoelastic energy dissipation at the crack tip and velocity dependence of the adhesive bond-breaking process, with a combined scaling exponent $\alpha \approx 0.33$.

\begin{figure}
	\centering
	\includegraphics[]{Figure09.pdf}
	\caption{Radial distributions calculated from the normal stress distribution measurements in figure~\ref{fig:spherestress}. Dashed lines show the velocity-dependent JKR model predictions from equations (\ref{eq:JKR}) and (\ref{eq:velocitydependence}) with $R = 24$~mm, $E = 3$~MPa, \hl{$W_0 = 70$~mJ~m$^{-2}$, $v_0 = 5\times10^{-5}$~m~s$^{-1}$, and $\alpha = 0.42$.} $a$ was set to the locations of the minima of the experimental distributions ($a = 1.38$~mm, 1.11~mm, 0.90~mm, and 0.60~mm for 0--83~ms), \hl{and $v = |\dot{a}|$ was calculated using finite difference approximations ($v = 3.6$~mm~s$^{-1}$, 10~mm~s$^{-1}$, 16~mm~s$^{-1}$, and 27~mm~s$^{-1}$ for 0--83~ms). Additional distributions recorded at intermediate times ($t = -17$~ms, 17~ms, and 33~ms) were used to fit the parameters $v_0$ and $\alpha$ but are not shown for simplicity.}}
	\label{fig:JKRmodel}
\end{figure}

	As shown in figure~\ref{fig:JKRmodel}, there is good qualitative agreement with the model, although the sensor tends to overestimate large compressive stresses, as discussed in section~\ref{sec:accuracy}. The errors seen in figure~\ref{fig:JKRmodel} for compressive stresses are consistent with the results of the accuracy test in figure~\ref{fig:accuracyhysteresis}(\textit{a}).