\section{Introduction}
\label{sec:introduction}

\subsection{The gecko adhesive system}

	The ability of geckos to support large loads with the adhesive pads on their toes is well known. Their adhesives have many interesting and desirable properties, including directionality, controllability, ease of attachment and detachment, self-cleaning, and adhesion on a wide range of surfaces \cite{Autumn2000,AutumnPeattie2002,Autumn2006,Autumn2006b,HansenAutumn2005,GravishWilkinsonAutumn2008,Gillies2014}. A large variety of gecko-inspired adhesives have been created in attempts to replicate these properties \cite{SameotoMenon2010}. For practical applications of these adhesives at large scales, it is important to consider the distribution of loads over macroscopic areas. Gecko toe morphology has provided bioinspiration for load-sharing mechanisms \cite{Hawkes2013,Hawkes2014}, but the actual distribution of loads within gecko toes has not been investigated experimentally.

	The toes of geckos are covered in flaps of skin called lamellae (or scansors), which are in turn covered in arrays of microscopic adhesive setae \cite{Russell1981,Russell1986}. The setae produce adhesion through van der Waals forces by achieving very close contact between the tips of the setae and the substrate \cite{Autumn2002}, and this adhesion can be controlled by changing the angle of the setae due to their asymmetrical shape \cite{Autumn2000}.

	The adhesive force is ultimately produced by the tips of the setae, but as noted by Russell \cite{Russell1975}, many authors emphasize the setae at the expense of an understanding of the gross anatomy of the toes and feet. The larger structures of the gecko's toe are equally important for the performance of the whole system. For maximum adhesion, the lamellae must conform to large-scale roughness of the substrate so that as many setae as possible can come into contact. \hl{To conform to the substrate, the gecko uses a ``hydrostatic skeleton'' to increase the area of contact by applying pressure over the whole adhesive pad during attachment.} In the tokay gecko (\emph{Gekko gecko}), the hydrostatic skeleton consists of a network of blood-filled sinuses within the bases of the lamellae (figure~\ref{fig:geckocrosssection}), which are connected to a central reservoir \cite{Russell1981}. Russell suggests that the tokay gecko may be able to actively or reflexively control the pressure in its blood sinuses using a sphincter-like muscle \cite{Russell1981}.

\begin{figure}
	\centering
	\includegraphics[]{figures/GeckoCrossSection.pdf}
	\caption{Cross section of the adhesive pad of a tokay gecko, showing anatomical features of the lamellae (after \cite{Russell1986}).}
	\label{fig:geckocrosssection}
\end{figure}

	After the setae make contact with the substrate, the load is transferred through a ``tensile skeleton'' consisting of branched tendons running along the lateral sides of the phalanges (figure~\ref{fig:geckotendons}) which are directly attached to the skin of the lamellae \cite{Russell1975,Russell1986}. These lateral digital tendons are anchored to the metatarsophalangeal joint capsules of each toe, which are connected to the flexor muscles and to each other by tendinous webs \cite{Russell1975}.

	Several authors have suggested that load-sharing is necessary in adhesive systems to provide robust adhesion and prevent load concentrations \cite{YaoGao2006,Kim2007,BullockFederle2011,Federle2006,ChenWuGao2008,Hawkes2013}. The tokay gecko appears to have sufficient muscle control to achieve active load-sharing between toes \cite{Russell1993}, but any load-sharing within the toes must be largely passive as the branches of the lateral digital tendons cannot be controlled independently.

	It is unknown to what extent load-sharing occurs within the toes. The skin of the lamellae has a limited extensibility \cite{Russell1986}, and the setae also produce stable forces during sliding \cite{GravishWilkinsonAutumn2008}, so it is possible that the gecko may take advantage of stretching or sliding of lamellae to produce a more uniform distribution of loads within each lamella and between independent lamellae. Sliding has been theoretically examined as a mechanism of load-sharing within the spatular tips of setae \cite{Cheng2012}, and it is possible that a similar effect may take place at the lamella scale.

\begin{figure}
	\centering
	\includegraphics[]{figures/GeckoTendons.pdf}
	\caption{Diagram showing the branching lateral digital tendons of the tokay gecko. Reprinted with permission from \cite{Russell1975}.}
	\label{fig:geckotendons}
\end{figure}

\subsection{Measurement of gecko adhesion}

	The tokay gecko is capable of supporting many times its body mass when placed in contact with a flat surface and pulled off either in normal or in shear directions \cite{Irschick1996, Pugno2011}. However, it has been observed \cite{Autumn2006, AutumnPeattie2002} that the forces produced by a gecko's feet and toes are many times lower than would be predicted from measurements taken on single setae and mm-scale arrays of setae \cite{Irschick1996, HansenAutumn2005, Autumn2002, Autumn2000}. This indicates that the tokay adhesive system becomes less area-efficient as the size of the system increases. A similar trend has been observed in synthetic systems as well \cite{Hawkes2014}. Since synthetic systems are now being developed that exceed the adhesive area of a gecko \cite{Hawkes2014, Parness2013, Bartlett2012}, it is increasingly important to identify the causes of inefficiency and avoid them.

	The cause of this decrease in adhesive stress as area increases in the tokay gecko is unknown. It has been suggested that inefficiency may be caused by incomplete area of contact or nonuniform distribution of loads, i.e. unequal load-sharing \cite{Autumn2006, BullockFederle2011}, but no evidence has been reported. To investigate this question further, we conducted \emph{in vivo} measurements of stress distribution and contact area on tokay gecko toes. A custom tactile sensor was developed for this purpose with high spatial resolution (100~$\mu$m) since the spacing between lamellae is too small to be resolved by most sensors (about 0.7~mm \cite{Gillies2014}).

	Section~\ref{sec:TactileSensor} describes the design and implementation of the custom tactile sensor. In section~\ref{sec:SensorTesting}, we characterize the sensor's accuracy and hysteresis and check the validity of the sensor readings by reproducing previous results for stress distributions on a strip of tape and an elastic sphere. The measurements of gecko stress distributions and contact areas are presented and discussed in sections~\ref{sec:GeckoMeasurements} and~\ref{sec:Discussion}.