\section{Gecko Measurements}
\label{sec:GeckoMeasurements}

	Having performed the tests described in section~\ref{sec:SensorTesting}, we now investigate the gecko's toe using the FTIR tactile sensor. The tactile sensor measures normal stress only, but the shear force produced by the gecko is also of interest. To measure shear force, we mounted the tactile sensor on an ATI Gamma 6-axis force-torque sensor which recorded data simultaneously with the tactile sensor. While this does not make a distributed shear stress measurement possible, we can use it to measure the total normal and shear forces and compare normal stress distributions at different total load configurations.

\subsection{Experimental procedure}

	The surface of the tactile sensor was oriented vertically, and an adult tokay gecko of body mass 101.2~g was held manually next to the sensor with its head downwards and was allowed to freely attach one of its rear feet to the sensor surface. In its natural posture while hanging head downwards, the gecko rotated its foot with the postaxial side oriented medially (i.e.\ fifth toe towards the midline, 180~degrees from standard anatomical position). The gecko was positioned so that its tail, belly, and other feet rested on a plate that was mechanically isolated from the sensor (see figure~\ref{fig:geckosketch}). Thus the total force measured by the ATI Gamma sensor was influenced by the desired foot only.

\begin{figure}
	\centering
	\includegraphics[]{figures/GeckoSketch.pdf}
	\caption{Sketch of the gecko measurement apparatus. Inset: image data from the camera showing one of the gecko's toes under load.}
	\label{fig:geckosketch}
\end{figure}

	Once the gecko was attached, we recorded the normal stress distribution and total normal and shear forces while the animal hung under its own weight. Then we pulled the body of the gecko downward and outwards by hand to produce \hl{higher normal and shear stresses} on the adhesive surface until detachment occurred. \hl{A total of 17 trials were performed in which the gecko attached to the sensor and was pulled off.} The loading angle was varied within trials to obtain different combinations of normal and shear force. \hl{Here the loading angle is defined as the angle that the total force vector makes with the sensor surface, so a loading angle of 0~degrees is a pure shear load and a loading angle of 90~degrees is a pure normal tensile load.} We took care not to injure the gecko in this experiment.

	\hl{When very large forces were applied to the gecko, some taxels became delaminated from the waveguide under the areas that were subjected to the highest stresses. The sensor data were discarded whenever this happened. The sensor was recalibrated between trials to minimize the effect of hysteresis and to reset any delaminated taxels. Due to the adhesion between the PVC cover membrane and the PDMS sensing membrane, the cover membrane was able to sustain the gecko's adhesive forces without stretching or deforming under most circumstances, but slight deformations in small areas did occur when the sensor was overloaded to the point of delamination. The cover membrane was not changed between trials, but the recalibration of the sensor allowed us to compensate for changes in sensitivity caused by the minor damage to the cover membrane.}

\subsection{Stress distribution measurements}
\label{sec:stressmeasurements}

	Selected stress distribution measurements \hl{from 6 of the trials} are shown in figure~\ref{fig:toearray}. All 5 toes of the foot were in contact with the sensor, but in many trials not all toes produced adhesion. The 3rd and 4th toes were best aligned with the direction of the load, so they supported more load than the other toes. The distributions in figure~\ref{fig:toearray} show the 3rd toe under different loading conditions. 

%movie 1: 4 trials
%movie 2: 3 trials
%movie 3: 4 trials
%movie 4: 3 trials
%movie 5: 3 trials

\begin{figure}
	\centering
	\includegraphics[]{figures/ToeArray2.pdf}
	\caption{Normal stress distributions for the 3rd toe of the right hind foot of a tokay gecko measured using the FTIR tactile sensor (calibrated range $-80$ to $+12$~kPa). \hl{Stress distributions are plotted at selected points in time from 6 independent trials. In each trial, the gecko attached to the sensor voluntarily and was pulled off by hand. The observed stress distributions vary with time as the load increases.}}
	\label{fig:toearray}
\end{figure}

	There is variation in the stress distributions between trials and between different loads from a single trial, but in all cases \hl{the distributions were nonuniform}. Concentrated compressive stresses were observed underneath the claw in most but not all trials. In the adhesive pad area, a periodic variation was observed along the proximodistal axis, indicating tensile stresses on the lamellae while the stresses in between lamellae were closer to zero. Additional variation was observed along the mediolateral axis, indicating that the stresses varied within lamellae as well as between lamellae.
	
	These results suggest that setae in some parts of the lamellae remained ``inactive,'' i.e.\ not producing adhesion, \hl{despite the net normal and shear forces on the toe.} In some cases, these inactive setae appear to be in a state of compressive normal stress, even though the net normal force was tensile. 

\subsection{Effect of loading angle on stress distribution}

	Using the data from the ATI sensor, we investigated whether the stress distribution is affected by the loading configuration applied to the toe. The loading angle in one of the trials is plotted versus time in figure~\ref{fig:loadanglevstime}. In this trial, a single toe supported the majority of the load as the loading angle (applied by hand) was increased gradually until the toe detached and slipped. The loading angle reached a maximum of about 21~degrees before detachment occurred; this is close to the 25.5~degree critical angle of detachment measured by Autumn \emph{et al.} \cite{Autumn2006b} for gecko toes.

\begin{figure}
	\centering
	\includegraphics[]{figures/LoadAngleVsTime.pdf}
	\caption{Load magnitude and loading angle vs.\ time measured using the ATI Gamma force sensor. Points (\textit{a})--(\textit{f}) correspond to the distributions shown in figures~\ref{fig:normalforcevsloadangle}(\textit{a})--(\textit{f}). The red dashed line indicates the time at which the gecko's toe detached. Force data are not reliable after this point (shaded region).}
	\label{fig:loadanglevstime}
\end{figure}

	The stress distribution on the load-bearing toe during this trial is plotted for a few points in time in figures~\ref{fig:normalforcevsloadangle}(\textit{a})--(\textit{f}) (see supplementary movie 1). Both compressive and tensile stresses were present at low loading angles (figures~\ref{fig:normalforcevsloadangle}(\textit{a})--(\textit{c})), but the tensile stresses became more prominent at higher loading angles (figures~\ref{fig:normalforcevsloadangle}(\textit{d})--(\textit{f})). To illustrate this more clearly, we integrated the negative and positive parts of the stress distributions to measure the total amount of tensile normal force and compressive normal force over the toe. These forces are plotted against the loading angle in figure~\ref{fig:normalforcevsloadangle} (right). The data show that the total tensile force increased in magnitude as the loading angle increased, while the total compressive force decreased in magnitude. The sum of the compressive and tensile forces appears to be positive (compressive) for figures~\ref{fig:normalforcevsloadangle}(\textit{a})--(\textit{c}), but this is an error caused by the sensor's inaccuracy when measuring large compressive stresses (section~\ref{sec:accuracy}). In fact, the net normal force measured by the ATI sensor was tensile throughout the trial.

\begin{figure}
	\centering
	\includegraphics[]{figures/NormalForceVsLoadAngle.pdf}
	\caption{Normal stress distributions and total forces at different loading angles. (\textit{a})--(\textit{f}) Normal stress distributions measured using the FTIR tactile sensor (calibrated range $-80$ to $+12$~kPa) at the six points in time indicated in figure~\ref{fig:loadanglevstime}(\textit{a})--(\textit{f}). Right: total compressive and tensile normal forces calculated by integrating the positive and negative parts of the stress distribution separately, \hl{plotted at 0.083~s intervals from $t=0$ to $1.833$~s.} loading angles from figure~\ref{fig:loadanglevstime} are plotted on the $x$-axis. Dashed lines and larger circles indicate the data corresponding to (\textit{a})--(\textit{f}).}
	\label{fig:normalforcevsloadangle}
\end{figure}

\subsection{FTIR measurement of contact area}
\label{sec:contactarea}

	In previous work, setal area on gecko toes has been measured by placing the toe in contact with glass and scanning or taking photographs \cite{Autumn2002,HansenAutumn2005,Irschick1996}, which measures the apparent area of the adhesive pad but does not determine the true area of contact. Additionally, the real contact area cannot be derived from the stress distribution measurements in section~\ref{sec:stressmeasurements}, since it is possible for setae to be in contact without producing a significant normal force.

	To measure the contact area, we removed the microtextured sensing membrane from the tactile sensor and placed the gecko's foot on the bare surface of the waveguide. The contacting setae gave off light through FTIR, while the non-contacting regions were much darker, as seen in figure~\ref{fig:contactarea} (see supplementary movie 2). This technique has previously been used to measure contact area in gecko-inspired adhesives \cite{Gravish2010,LeeFearingKomvopoulos2008,GilliesFearing2011,Lee2009} and to observe the process of detachment in live geckos \cite{Autumn2014}.

\begin{figure}
	\centering
	\includegraphics[]{figures/ContactArea.pdf}
	\caption{Images of the right hind foot of a tokay gecko on the FTIR waveguide with the sensing membrane removed. Bright regions indicate contacting setae. Toes 1--5 are numbered. (\textit{a})--(\textit{c}) Image sequence showing increase in contact area as the foot comes into contact and load is applied. \hl{Dark patches within the lamellae of toe 3 indicate that small sections of the lamellae do not make contact with the surface due to wrinkling.} (\textit{d})--(\textit{h}) Sequence showing severe lamellar wrinkling associated with slow steady-state sliding. \hl{The lateral sides of toe 4 move towards each other as the toe slides, causing the lamellae to become folded down the middle of the toe and lose contact with the surface.}}
	\label{fig:contactarea}
\end{figure}

	The area of contact increases substantially when the toes are pressed, as shown in figure~\ref{fig:contactfingerpress}(\textit{a})--(\textit{c}) (see supplementary movie 3). The total contact area was measured by identifying contacting setae with a quadratic classifier (figure~\ref{fig:contactfingerpress}(\textit{d})--(\textit{f})). The areas before, during, and after pressing the toe were 7.2~mm$^2$, 14.4~mm$^2$, and 5.1~mm$^2$, indicating that some setae spontaneously detached from the surface even after being pressed into contact.

\begin{figure}
	\centering
	\includegraphics[]{figures/ContactFingerPress.pdf}
	\caption{Change in contact area caused by applying pressure to the dorsal surface of the 5th toe while under load. \hl{The toe was pressed with a force of 0.2 N and then released.} In (\textit{a})--(\textit{c}), the toe is shown before (\textit{a}), during (\textit{b}), and after application of pressure (\textit{c}). \hl{Dark patches in the center of the toe indicate that small sections of the lamellae do not make contact due to wrinkling, even when the toe is pressed.} In (\textit{d})--(\textit{f}), these images are analyzed with a quadratic classifier to distinguish contacting areas (white) from non-contacting areas (gray).}
	\label{fig:contactfingerpress}
\end{figure}